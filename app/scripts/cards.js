console.log('Cargando Cards...');

const dataCards = [{
    "title": "Tarantula Goliath",
    "url_image":"https://wallpapercave.com/wp/wp2060788.jpg",
    "desc":"Tarantula Goliath: Hembra a 15$ y Macho $ 10",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Arañas Latigo",
    "url_image":"https://elements-video-cover-images-0.imgix.net/files/7f54f7f8-2b7b-4220-8acb-e4b8fbcccb74/inline_image_preview.jpg?auto=compress&crop=edges&fit=crop&fm=jpeg&h=630&w=1200&s=d8cd00ab2f95884a21a006b2b9991079",
    "desc":"Araña Latigo: Hembra a 40$ y Macho 50$",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Scorpiones",
    "url_image":"https://cdn.branchcms.com/78mAr2bA1J-1200/images/scorpions-inhabiting-florida.jpg",
    "desc":"Scorpion: Hembra a 25$ y Macho 15$",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Pitones Bola",
    "url_image":"https://i.pinimg.com/originals/89/4c/52/894c528d4a565be5c2bd38f76335ee0c.jpg",
    "desc":"Pitones de los 20$ a los 15000$",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Boas",
    "url_image":"https://cumbrepuebloscop20.org/wp-content/uploads/2018/11/constrictor-2808200_640.jpg",
    "desc":"Boas de los 20$ a los 500$",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Otras Serpientes",
    "url_image":"https://okdiario.com/img/2017/04/26/serpientes-mas-increibles-mundo-3-655x368.jpg",
    "desc":"otras serpientes para tu agrado baratas",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Geckos Leopardo Raptor",
    "url_image":"https://www.gecko-leopardo.com/wp-content/uploads/2020/01/Gecko-leopardo-Raptor-foto.jpg",
    "desc":"Geckos leopardo: Hembra a 40$ y Macho 50$",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Iguanas",
    "url_image":"https://deiguana.com/wp-content/uploads/2019/05/iguana-verde.jpg",
    "desc":"Iguanas Hembra a 150$ y Macho 100$",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    },
    {
    "title": "Camaleones",
    "url_image":"https://culturacientifica.com/app/uploads/2016/01/camaleones.jpg",
    "desc":"Camaleones: Ha mitad de precio 50% ",
    "cta":"Mostrar mas",
    "link":"https://salvajes.es/"
    }];

    (function () {
        let CARD = {
            init: function (){
                //console.log('card module was loaded')
                let _self = this;

                //llamamos las funciones
                this.insertData(_self);
                //this.eventHandler(_self)
            },

            eventHandler: function (_self) {
                let arrayRefs = document.querySelectorAll('.accordion-title');

                for (let x = 0; x < arrayRefs.length; x++) {
                    arrayRefs[x].addEventListener('click', function(event){
                        console.log('event', event);
                        _self.showTab(event.target);
                    });
                }
            },

            insertData: function (_self) {
                dataCards.map(function(item, index) {
                    document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item,index));
                });
            },

            tplCardItem: function (item, index) {
                return(`<div class='card-item' id="card-number-${index}">
                <img src="${item.url_image}"/>
                <div class="card-info">
                <p class='card-title'>${item.title}</p>
                <p class='card-desc'>${item.desc}</p>
                <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
                </div>
                </div>`)},
            }
            CARD.init();
        })();