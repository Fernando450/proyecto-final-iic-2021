console.log('Cargando Acccordion...');

const dataAccordion = [{
    "title": "¿Informacion sobre los reptiles?",
    "desc": "Los reptiles son generalmente carnívoros pero los hay también herbívoros y omnívoros. Todos son ovíparos, nacen de un huevo. La única especie que no tiene cuatro patas es la de las serpientes. La mayoría son cazadores y algunos venenosos o constrictores, por lo que son animales exóticos y son mascotas domesticas."
}];

(function () {
    let ACCORDION = {
        init: function () {
            let _self = this;
            //llamamos las funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event) {
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        showTab: function (refItem) {
            let activeTab = document.querySelector('.tab-active');

            if (activeTab){
                activeTab.classList.remove('tab-active');
            }

            console.log ('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },

        insertData: function (_self) {
            dataAccordion.map (function (item, index){
                //console.log(item¡¡¡¡¡, item);
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },

        tplAccordionItem: function (item) {
            return (`<div class='accordion-item'>
            <p class='accordion-title'>${item.title}</p>
            <p class='accordion-desc'>${item.desc}</p>
            </div>`)},
        }

        ACCORDION.init();
    })();
